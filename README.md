Clone this repo.

Install `docker`, `docker-compose`.

Run sudo su.

Run  
`docker-compose build`,

`docker-compose up -d`,

`docker-compose exec php7 bash`

if first deploy app, run `composer install` and wait for all dependencies to be install.

After check if all is good. 
Run 

`bin/console app:image-save https://pixabay.com/ru/images/search/cars/`

after run this command, in folder `app/images` will be created folder with name your site. This folder will contain all the images that were found.