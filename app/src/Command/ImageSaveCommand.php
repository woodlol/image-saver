<?php

namespace App\Command;

use App\Component\ImageSaveHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImageSaveCommand extends Command
{
    /** @var ImageSaveHandler  */
    private $imgSaveHandler;

    /** @var string  */
    protected static $defaultName = 'app:image-save';

    /**
     * @param ImageSaveHandler $handler
     */
    public function __construct(ImageSaveHandler $handler)
    {
        parent::__construct();

        $this->imgSaveHandler = $handler;
    }

    protected function configure()
    {
        $this
            ->setDescription('Parse main page site url which u write here.')
            ->addArgument('url', InputArgument::OPTIONAL, 'url')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io   = new SymfonyStyle($input, $output);
        $url  = $input->getArgument('url');

        if (null === $url) {
            $io->writeln('Enter some site with images.');
        }

        try {
            $this->imgSaveHandler
                ->getImagesFromUrl($url)
                ->saveFiles($url);

            $io->note(sprintf('You site parsed: %s', $url));
        } catch (\Throwable $exception) {
            echo $exception->getMessage();
        }
    }
}
