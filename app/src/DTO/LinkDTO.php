<?php

namespace App\DTO;

class LinkDTO
{
    /** @var string */
    public $name;

    /**
     * @param string $link
     */
    public function __construct(string $link)
    {
        $this->name = $link;
    }

    /**
     * @return bool
     */
    public function isValidName(): bool
    {
        return substr($this->name, 0, 4) === 'http';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        preg_match('/^.+\/(.+)$/', $this->name, $matched);

        $name = 'default' . rand();

        if (isset($matched[1])) {
            $name = $matched[1];
        }

        return $name;
    }
}
