<?php

namespace App\DTO\Collection;

use App\DTO\LinkDTO;

class LinkCollection extends TypedCollection
{
    public function __construct(array $elements = [])
    {
        parent::__construct(LinkDTO::class, $elements);
    }
}
