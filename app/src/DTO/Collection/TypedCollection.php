<?php


namespace App\DTO\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use Webmozart\Assert\Assert;

class TypedCollection extends ArrayCollection
{
    /** @var string */
    protected $type;

    /**
     * @param string $class
     * @param array  $elements
     */
    public function __construct(string $class, array $elements = [])
    {
        $this->type = $class;

        Assert::classExists($class);
        Assert::allIsInstanceOf($elements, $this->type);

        parent::__construct($elements);
    }

    /**
     * {@inheritdoc}
     */
    public function add($element): bool
    {
        Assert::isInstanceOf($element, $this->type);

        return parent::add($element);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key)
    {
        Assert::isInstanceOf($this->get($key), $this->type);

        return parent::remove($key);
    }
}
