<?php

namespace App\Component;

use App\DTO\Collection\LinkCollection;
use App\DTO\LinkDTO;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;

class SiteParser
{
    public const XPATH_TO_TAG_IMG  = './/img';

    /** @var Client  */
    private $client;

    /** @var Crawler  */
    private $crawler;

    public function __construct()
    {
        $this->client  = new Client();
        $this->crawler = new Crawler();
    }

    /**
     * @param string $url
     * @param string $xpath
     *
     * @throws GuzzleException
     * @throws Exception
     *
     * @return LinkCollection
     */
    public function getAllImages(string $url, string $xpath = './/img'): LinkCollection
    {
        $linkCollection = new LinkCollection();

        $this->crawler->addHtmlContent($this->getHtmlByUrl($url));

        foreach ($this->crawler->filterXPath($xpath)->getIterator() as $item) {
            $linkCollection->add(new LinkDTO($item->getAttribute('src')));
        }

        if ($linkCollection->isEmpty()) {
            throw new Exception($url . ' have not image with xpath ' . $xpath);
        }

        return $linkCollection;
    }

    /**
     * @param string $url
     *
     * @throws GuzzleException
     *
     * @return string
     */
    private function getHtmlByUrl(string $url): string
    {
        return $this->client->request(Request::METHOD_GET, $url)
            ->getBody()
            ->getContents();
    }
}
