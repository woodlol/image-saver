<?php

namespace App\Component;

use App\DTO\Collection\LinkCollection;
use App\DTO\LinkDTO;
use Symfony\Component\Filesystem\Filesystem;

class ImageSaveHandler
{
    public const DEFAULT_FOLDER = 'images';

    /** @var SiteParser  */
    private $parser;

    /** @var Filesystem  */
    private $fileSystem;

    /** @var LinkCollection */
    private $links;

    /** @var string */
    private $path;

    /**
     * @param SiteParser $siteParser
     * @param string $path
     */
    public function __construct(SiteParser $siteParser, string $path)
    {
        $this->parser     = $siteParser;
        $this->fileSystem = new Filesystem();
        $this->path       = $path;
    }

    /**
     * @param string $url
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     * @return self
     */
    public function getImagesFromUrl(string $url): self
    {
        $this->links = $this->parser->getAllImages($url);

        return $this;
    }

    /**
     * @param string $folder
     */
    public function saveFiles(string $folder): void
    {
        $path = $this->path . DIRECTORY_SEPARATOR . self::DEFAULT_FOLDER . DIRECTORY_SEPARATOR . self::cleanUrl(urldecode($folder));

        $this->fileSystem->mkdir($path);

        /** @var LinkDTO $link */
        foreach ($this->links->getValues() as $link) {
            if (!$link->isValidName()) {
                continue;
            }

            $name     = $link->getName();
            $filename = $path . DIRECTORY_SEPARATOR . $name;

            if (!$this->fileSystem->exists($filename)) {
                copy($link->name, $filename);
            }
        }
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public static function cleanUrl(string $url): string
    {
        $url = str_replace('/', '|', $url);

        return $url;
    }
}
